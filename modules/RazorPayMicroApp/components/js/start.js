class Start{
    run(intentData, sessionData){
        var data = {
            card:{
                name:"Akshay",
                number:4111111111111111,
                cvv:123,
                expiry_month:12,
                expiry_year:18,
            },
            amount:1000,
            currency:"INR",
            email:"foo@bar.com",
            contact:9090909090,
            method:"card",
            ip:"192.168.0.20",
            referer:"mozilla/5.0",
            user_agent:"mozilla/5.0",
            callback_url:"https://production.appsfly.io/paytm/posted-params"
        };
        intentData = data;
        return new Intent("next", intentData);
    }
}

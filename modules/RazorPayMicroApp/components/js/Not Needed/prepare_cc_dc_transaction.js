/**
 * Created by karthik on 16/11/16.
 */
class PaytmPreProcessingOrder_CC_DC{
	run(intentData, sessionData){
		intentData.checksum_params = {
			REQUEST_TYPE:"PAYTM_EXPRESS",
			ORDER_ID:intentData.saveAttendee.eventSignupId,
			CUST_ID:intentData.saveAttendee.userDetails.id,
			TXN_AMOUNT:intentData.totalBillable,
			MSISDN:sessionData.paytm_number,
			EMAIL:intentData.saveAttendee.userDetails.email,
			PAYMENT_DETAILS:intentData.card_token.TOKEN,
			AUTH_MODE:"3D",
			addMoney:0,
			IS_SAVED_CARD:1,
			PAYMENT_TYPE_ID:"CC",
			PAYTM_TOKEN:sessionData.paytm_token.PAYTM_TOKEN,
			MID:sessionData.paytm_MID,
			CHANNEL_ID:"WEB",
			INDUSTRY_TYPE_ID:"Retail104",
			THEME:"Merchant",
			WEBSITE:"Meraevents",
			CALLBACK_URL:"http://107.170.249.109:5000/post-params"
		};
		return new Intent("next", intentData);
	}
}

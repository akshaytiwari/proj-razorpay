/**
 * Created by bhaskar_q on 27/10/16.
 */
class PaytmOTPParamBuilder{
	run(intentData, sessionData){
		var mobile = null;
		if(intentData.mobile){
			mobile = intentData.mobile;
		}
		else{
			mobile = sessionData.paytm_number;
		}
		intentData.paytm_otp = "";
		intentData.checksum_params = {
			PHONE:mobile,
			USER_TYPE:"00",
			RESPONSE_TYPE:"token",
			SCOPE:"paytm,txn",
			MID:sessionData.paytm_MID,
			OTP_DELIVERY_METHOD:"SMS"
		};
		return new Intent("next", intentData);
	}
}
/**
 * Created by bhaskar_q on 27/10/16.
 */
class PaytmNBParams{
	run(intentData, sessionData){
		var data = intentData;
		var body = "";
		_.each(data.paytm_wallet_pay_res, function(item, key){
			if(!body){
				body = "paytmPostParams["+key+"]="+encodeURIComponent(item);
				body = body+"&"+key+"="+encodeURIComponent(item);
			}
			else{
				body = body+"&paytmPostParams["+key+"]="+encodeURIComponent(item);
				body = body+"&"+key+"="+encodeURIComponent(item);
			}
		});
		body = body+"&orderId="+data.saveAttendee.orderId;
		body = body+"&paymentGatewayKey=6";
		body = body+"&eventSignup="+data.checksum.ORDER_ID;
		body = body+"&mobile="+sessionData.paytm_number;
		body = body+"&email="+data.saveAttendee.userDetails.email;
		intentData.paytm_res = {
			data:body
		};
		return new Intent("next", intentData);
	}
}
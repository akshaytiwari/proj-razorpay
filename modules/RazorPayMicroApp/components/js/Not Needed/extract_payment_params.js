/**
 * Created by bhaskar_q on 27/10/16.
 */
class ExtractPaymentParams{
	run(intentData, sessionData){
		intentData.paytm_wallet_pay_res={};
		var callback_url = intentData.web_response;
		var callback_params = callback_url.split("?")[1];
		_.each(callback_params.split("&"),function(item){
			var key = decodeURIComponent(item.split("=")[0]);
			var value = decodeURIComponent(item.split("=")[1]);
			intentData.paytm_wallet_pay_res[key]=value;
		});
		return new Intent("next", intentData);
	}
}
/**
 * Created by bhaskar_q on 27/10/16.
 */
class PaytmProcessVerification{
	run(intentData, sessionData){
		if(intentData.paytm_token.TOKEN_DETAILS){
			if(intentData.checksum.PHONE){
				sessionData.paytm_number = intentData.checksum.PHONE;
			}
			sessionData.paytm_token = intentData.paytm_token.TOKEN_DETAILS;
			return new Intent("next", intentData);
		}
		else{
			intentData.paytm_error =intentData.paytm_token.ErrorMsg;
			return new Intent("wrong_crediential", intentData);
		}
	}
}
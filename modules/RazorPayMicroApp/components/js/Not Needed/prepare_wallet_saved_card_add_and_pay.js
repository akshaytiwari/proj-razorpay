/**
 * Created by bhaskar_q on 16/11/16.
 */
class PaytmPreProcessingOrder_SC{
	run(intentData, sessionData){
		intentData.checksum_params = {
			REQUEST_TYPE:"PAYTM_EXPRESS",
			ORDER_ID:intentData.saveAttendee.eventSignupId,
			CUST_ID:intentData.saveAttendee.userDetails.email,
			TXN_AMOUNT:intentData.totalBillable,
			MSISDN:sessionData.paytm_number,
			EMAIL:intentData.saveAttendee.userDetails.email,
			PAYMENT_DETAILS:"200bf5a043d401d36ea9daf0f4bed28d2d771a8e36a634b445793b2aed2621f6",
			AUTH_MODE:"3D",
			PAYMENT_TYPE_ID:"CC",
			ADD_MONEY:intentData.totalBillable-intentData.paytm_balance,
			IS_SAVED_CARD:1,
			SSO_TOKEN:sessionData.paytm_token.TXN_TOKEN,
			PAYTM_TOKEN:sessionData.paytm_token.PAYTM_TOKEN,
			MID:sessionData.paytm_MID,
			CHANNEL_ID:"WAP",
			INDUSTRY_TYPE_ID:"Retail104",
			THEME:"Merchant",
			WEBSITE:"Meraevents",
			CALLBACK_URL:"http://107.170.249.109:5000/post-params"
		};
		return new Intent("next", intentData);
	}
}


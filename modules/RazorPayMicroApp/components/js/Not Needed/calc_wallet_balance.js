/**
 * Created by bhaskar_q on 27/10/16.
 */
class PaytmBalanceSave{
	run(intentData, sessionData){
		if(intentData.paytm_balance.statusCode==408 || intentData.paytm_balance.statusCode==403){
			return new Intent("login", intentData);
		}
		else{
			sessionData.paytm_balance = intentData.paytm_balance.response;
			intentData.rem_billable = intentData.calculations.response.calculationDetails.totalPurchaseAmount - intentData.paytm_balance.response.amount;
			intentData.totalBillable = intentData.calculations.response.calculationDetails.totalPurchaseAmount;
			intentData.paytm_number = sessionData.paytm_number;
			return new Intent("next", intentData);
		}
	}
}